package com.simba.consts;

/**
 * Created by shuoGG on 2018/7/24
 */
public class PushType {
    public static final String JPUSH_NOTIFICATION = "PUSH_CENTER_JPUSH_NOTIFICATION";
    public static final String JPUSH_MESSAGE = "PUSH_CENTER_JPUSH_MESSAGE";
    public static final String WEBSOCKET =   "PUSH_CENTER_WEBSOCKET";
    public static final String SHORT_MESSAGE = "PUSH_CENTER_SHORT_MESSAGE";
    public static final String EMAIL = "PUSH_CENTER_EMAIL";
}

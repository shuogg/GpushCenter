package com.simba.service.sender;

/**
 * Created by shuoGG on 2018/7/24
 */
public interface ISender {
    void send(Long userId, String content);
}

package com.simba.config;

import com.simba.consts.GlobalValue;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by shuoGG on 2018/9/25
 */
@Configuration
public class RabbitConfig {

	@Bean
	public Queue emailQueue() {
		return new Queue(GlobalValue.QUEUE_NAME);
	}

}
